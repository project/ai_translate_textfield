<?php

declare(strict_types=1);

namespace Drupal\ai_translate_textfield\Utility;

/**
 * Utility class to detect HTML in a string.
 */
class HtmlDetector {

  /**
   * Determines if a given string contains HTML markup.
   *
   * @param string $text
   *   The input text to check.
   *
   * @return bool
   *   TRUE if the text contains HTML, FALSE otherwise.
   */
  public static function containsHtml(string $text): bool {
    // Strip all tags and compare the result with the original string.
    $stripped = strip_tags($text);

    // If stripped differs from the original text, it contains HTML.
    if ($stripped !== $text) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Checks if a string contains HTML entities.
   *
   * @param string $text
   *   The text to check.
   *
   * @return bool
   *   TRUE if the text contains HTML entities, FALSE otherwise.
   */
  public static function containsHtmlEntities(string $text): bool {
    // Check for entities like &lt; or &#123;.
    if (preg_match('/&[a-zA-Z0-9#]+;/', $text)) {
      return TRUE;
    }
    return FALSE;
  }

}
