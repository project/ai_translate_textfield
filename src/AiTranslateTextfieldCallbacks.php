<?php

namespace Drupal\ai_translate_textfield;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\ai_translate_textfield\Utility\HtmlDetector;
use Drupal\ai\AiProviderPluginManager;
use Drupal\ai\OperationType\Chat\ChatInput;
use Drupal\ai\OperationType\Chat\ChatMessage;
use Drupal\ai\OperationType\TranslateText\TranslateTextInput;
use Drupal\ai\Plugin\ProviderProxy;
use GuzzleHttp\Exception\GuzzleException;
use Soundasleep\Html2Text;

/**
 * Callbacks for the AI Textfield Translation module.
 */
final class AiTranslateTextfieldCallbacks implements TrustedCallbackInterface {

  /**
   * The supported field widgets.
   */
  public const SUPPORTED_FIELD_WIDGETS = [
    'string_textarea',
    'text_textarea',
    'string_textfield',
    'text_textfield',
    'text_textarea_with_summary',
  ];

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['ajaxTranslateText', 'processElement'];
  }

  /**
   * Process hook to add the translation feature for supported form elements.
   *
   * @param array|mixed $element
   *   The render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $context
   *   The context.
   *
   * @return array
   *   The processed render array.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array $context): array {
    // We're on another type of field.
    if (
        !isset($context['widget'])
        || !in_array($context['widget']->getPluginId(), self::SUPPORTED_FIELD_WIDGETS, TRUE)
    ) {
      return $element;
    }

    // Let's check if the feature is enabled for this field.
    if (!$context['widget']->getThirdPartySetting('ai_translate_textfield', 'enable_translations')) {
      return $element;
    }

    if (!\Drupal::currentUser()->hasPermission('use ai translation')) {
      return $element;
    }

    $config = \Drupal::config('ai_translate_textfield.settings');

    $langcode = $context['items']->getEntity()->language()->getId();
    if (isset($config->get('languages')[$langcode]['disabled']) && $config->get('languages')[$langcode]['disabled']) {
      return $element;
    }

    $fieldName = $context['items']->getName();
    $parents = implode('-', $element['#field_parents']);
    $id = ($parents ? $parents . '-ai-translator-' : 'ai-translator-') . $fieldName . '-' . $context['delta'];
    $element['#prefix'] = '<div class="' . $id . '">';
    $element['#suffix'] = '</div>';
    $settings = [
      'strip_tags' => $context['widget']->getThirdPartySetting('ai_translate_textfield', 'strip_tags'),
      'html' => str_starts_with($context['items']->getFieldDefinition()->getType(), 'text_'),
    ];

    $buttonText = $config->get('button_text') ?? t('Request automatic translation');

    if ($config->get('warning_enabled')) {
      $element['#attached']['library'][] = 'ai_translate_textfield/modal-button-action';
      $element['#attached']['drupalSettings']['ai_translate_textfield_modal'] = [
        'dialog_cancel_button' => $config->get('dialog_cancel_button') ?? 'Cancel',
        'dialog_content' => $config->get('dialog_content')['value'] ?? 'content missing',
        'dialog_ok_button' => $config->get('dialog_ok_button') ?? 'Ok',
        'dialog_title' => $config->get('dialog_title') ?? 'title missing',
      ];
      $element['translation_warning_button'] = [
        '#attributes' => [
          'class' => [
            'ai-translator-warning-button',
          ],
          'data-ai-translator-id' => $id,
        ],
        '#limit_validation_errors' => [],
        '#name' => $id . '-modal-button',
        '#type' => 'button',
        '#value' => $buttonText,
        '#weight' => 500,
      ];
      $element['translate_button'] = [
        '#ajax' => [
          'callback' => [self::class, 'ajaxTranslateText'],
          'event' => 'click',
          'wrapper' => $id . '-wrapper',
        ],
        '#attributes' => [
          'class' => [
            'hidden',
            'js-hide',
          ],
          'data-ai-translator' => $id,
        ],
        '#limit_validation_errors' => [],
        '#name' => 'translate_button-' . $id,
        '#type' => 'button',
        '#value' => $buttonText,
        '#weight' => 500,
        '#widget_settings' => $settings,
      ];

    }
    else {
      // Add a button to trigger translation with Ajax.
      $element['translate_button'] = [
        '#ajax' => [
          'callback' => [self::class, 'ajaxTranslateText'],
          'wrapper' => $id . '-wrapper',
        ],
        '#attributes' => [
          'data-ai-translator' => $id,
        ],
        '#limit_validation_errors' => [],
        '#name' => 'translate_button-' . $id,
        '#type' => 'button',
        '#value' => $buttonText,
        '#weight' => 500,
        '#widget_settings' => $settings,
      ];

    }

    return $element;
  }

  /**
   * Ajax callback to translate text and update the field value.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response.
   */
  public static function ajaxTranslateText(array &$form, FormStateInterface $formState): AjaxResponse {
    $element = $formState->getTriggeringElement();
    $parents = array_slice($element['#parents'], 0, -1);
    $array_parents = array_slice($element['#array_parents'], 0, -1);

    $widgetSettings = $element['#widget_settings'];

    $selector = $element['#attributes']['data-ai-translator'];

    // Get the current field value.
    $fieldValue = $formState->getValue(array_merge($parents, ['value']));

    $target_language = $formState->getFormObject()->getEntity()->language()->getId();

    try {
      $success = FALSE;
      // Translate the text using the selected service.
      $translated = self::translateText($fieldValue, $widgetSettings, $target_language);

      if ($fieldValue === $translated) {
        $messages = [
          MessengerInterface::TYPE_ERROR => [t(
              'The text and its translation are the same so maybe there was an error trying to translate it. Was the text already written on the current language?',
            ),
          ],
        ];
      }
      elseif (empty($translated)) {
        $messages = [
          MessengerInterface::TYPE_ERROR => [t(
              'The translation result is empty for an unknown reason. Not replacing the original content.',
            ),
          ],
        ];
      }
      else {
        $success = TRUE;
        $messages = [
          MessengerInterface::TYPE_STATUS => [
            [
              '#markup' => (string) t(
                  'Replaced text with an AI translation. Please review it thoroughly before saving. The original text was:'
              ) . '<br>' . $fieldValue,
              '#type' => 'markup',
            ],
          ],
        ];
      }
    }
    catch (\Throwable $e) {
      $translated = '';
      $messages = [
        MessengerInterface::TYPE_ERROR => [
          t('The translation failed and the service returned the following error: @error',
            ['@error' => $e->getMessage()],
          ),
        ],
      ];
    }

    $messages = [
      '#message_list' => $messages,
      '#theme' => 'status_messages',
      '#weight' => -1000,
    ];

    // The rendering system insist on not rendering groups again. That's cool,
    // but works against our purpose, if the element happens to be inside one.
    unset($form[$array_parents[0]]['#group']);

    NestedArray::setValue($form, array_merge($array_parents, ['messages']), $messages);

    // Only change the field value on success.
    if ($success) {
      $formState->setValue(array_merge($parents, ['value']), $translated);
      NestedArray::setValue($form, array_merge($array_parents, ['value', '#value']), $translated);
    }

    $changed = NestedArray::getValue($form, $array_parents);
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('.' . $selector, $changed));

    return $response;
  }

  /**
   * Translate text using the selected translation service.
   *
   * @param string $text
   *   The text to be translated.
   * @param array $settings
   *   Settings for this field.
   * @param string $targetLang
   *   The language code of the target language.
   *
   * @return string
   *   The translated text.
   */
  protected static function translateText(string $text, array $settings, string $targetLang): string {
    $config = \Drupal::config('ai_translate_textfield.settings');
    $languageConfig = $config->get('languages')[$targetLang] ?? [];
    $model = $languageConfig['model'] ?? '';

    if (empty($languageConfig)) {
      throw new \Exception('No languages configured. Please contact your administrator.');
    }

    if (empty($languageConfig['model'])) {
      throw new \Exception('No model configured for the target language. Please contact your administrator.');
    }

    if ($languageConfig['disabled']) {
      throw new \Exception('The target language is disabled. Please contact your administrator.');
    }

    $stripTags = $settings['strip_tags'];
    if ($stripTags) {
      $text = class_exists(Html2Text::class)
          ? Html2Text::convert($text)
          : strip_tags($text);
    }

    $translatorConfig = self::getProvider($targetLang);
    $translator = $translatorConfig['provider_id'];
    if (!$translator instanceof ProviderProxy) {
      throw new \Exception('No valid translator found.');
    }

    $supportedOperationTypes = $translator->getSupportedOperationTypes();
    if (in_array('translate_text', $supportedOperationTypes, TRUE)) {
      $text = new TranslateTextInput($text, NULL, $targetLang);

      $options = [];

      if ($settings['html'] && 'ai_provider_deepl' === $translator->getModuleDataName()) {
        $options['tag_handling'] = 'html';
      }

      $translation = $translator->translateText($text, $model, $options)->getNormalized();
    }
    // We're on 'chat' operation type.
    elseif (in_array('chat', $supportedOperationTypes, TRUE)) {
      $prompt = $languageConfig['prompt'];
      if (empty($prompt)) {
        throw new \Exception('No prompt configured for the target language. Please contact your administrator.');
      }
      // Check if the input text is containing HTML. This is a workaround for
      // some models that insists on escaping the HTML as HTML entities.
      $hasHtml = HtmlDetector::containsHtml($text);

      $promptText = \Drupal::service('twig')->renderInline($prompt, [
        'dest_lang' => $targetLang,
        'dest_lang_name' => \Drupal::languageManager()->getLanguage($targetLang)->getName(),
        'input_text' => $text,
      ]);
      try {
        $messages = new ChatInput([
          new ChatMessage('system', 'You are helpful translator.'),
          new ChatMessage('user', $promptText),
        ]);

        /** @var /Drupal\ai\OperationType\Chat\ChatOutput $message */
        $message = $translator->chat($messages, $translatorConfig['model_id'])->getNormalized();
      }
      catch (GuzzleException $exception) {
        // Error handling for the API call.
        throw new \Exception('An error occurred communicating to the AI service: ' . $exception->getMessage());
      }
      $translation = trim(trim(trim($message->getText(), '```'), ' '), '"');
      // The process encoded the markup to HTML entities, so we need to decode
      // them back.
      if ($hasHtml && !HtmlDetector::containsHtml($translation) && HtmlDetector::containsHtmlEntities($translation)) {
        $translation = htmlspecialchars_decode($translation);
      }
    }
    else {
      throw new \Exception('No valid operation type found.');
    }

    return $translation;
  }

  /**
   * Get the preferred provider if configured, else take the default one.
   *
   * @param string $langcode
   *   The language code we're translating to.
   *
   * @return array
   *   A translation provider.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   An exception.
   */
  private static function getProvider(string $langcode): array {
    $aiProviderManager = \Drupal::service('ai.provider');
    assert($aiProviderManager instanceof AiProviderPluginManager);
    $config = \Drupal::config('ai_translate_textfield.settings');
    $parts = explode('__', $config->get('languages')[$langcode]['model'] ?? '');
    $provider = NULL;
    if (count($parts) === 2) {
      $provider = $aiProviderManager->createInstance($parts[0]);
    }

    if (!$provider) {
      throw new \Exception('No valid provider found.');
    }
    return [
      'provider_id' => $provider,
      'model_id' => $parts[1],
    ];
  }

}
